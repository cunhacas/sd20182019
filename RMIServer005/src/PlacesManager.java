import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class PlacesManager extends UnicastRemoteObject implements PlacesListInterface, MonitoringInterface{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<Place> places = new ArrayList<Place>();
	
	public PlacesManager(int port) throws RemoteException {
		try {
			ReplicasManagementInterface plm  = (ReplicasManagementInterface) Naming.lookup("rmi://localhost:2024/replicamanager");
			String add = plm.addReplica("rmi://localhost:"+ port + "/placelist");
			if(add != null) {
				PlacesListInterface pl  = (PlacesListInterface) Naming.lookup(add);
				ArrayList<Place> places = pl.allPlaces();
			
				if(places != null)
					for(Place p: places) addPlace(p);
			}
			
		} catch (MalformedURLException | NotBoundException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void addPlace(Place p) throws RemoteException { 
		places.add(p);
	}
	
	public synchronized ArrayList<Place> allPlaces() throws RemoteException{
		return places;
	}

	@Override
	public synchronized Place getPlace(String id) throws RemoteException {
		int i=0;
		Place p;
		while(i< places.size()) 
			if((p=places.get(i++)).getPostalCode().equals(id)) return p;
		
		return null;
	}

	@Override
	public void ping() throws RemoteException {
		// TODO Auto-generated method stub
		
	}
}
