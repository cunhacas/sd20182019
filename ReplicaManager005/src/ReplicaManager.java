import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class ReplicaManager extends UnicastRemoteObject implements PlacesListManagerInterface, ReplicasManagementInterface{

	private static final long serialVersionUID = 1L;
	private final ReplicaManager r;
	private ArrayList<String> placesList = new ArrayList<String>();

	public ReplicaManager() throws RemoteException, InterruptedException {
		r = this;
		(new Thread() {
			public void run() {
				while(true) {
					// vamos utilizar o lock do ReplicaManager
					//se utilizarmos o "this" estamos a recorrer ao lock do object Thread
					synchronized(r) {  
						for(String s: placesList) {
							MonitoringInterface m = null;
							try {
								m = (MonitoringInterface) Naming.lookup(s);
								m.ping();
								System.out.println("ReplicaManager-->PING "+ s);
							}catch(Exception e) {
								placesList.remove(s);
								(new Thread() {
									public void run() {
										RMIServer.main(new String[]{s.split(":")[2].substring(0, 4)});
									}
								}).start();
								System.out.println("ReplicaManager-->RECUPEREI");
								break; //a lista foi modificada e por isso terminamos o FOR
							}
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}}}).start();

	}

	public synchronized void addPlace(Place p) throws RemoteException { 
		PlacesListInterface l;
		ObjectRegistryInterface r;
		try {
			for(String s:placesList) {
				l = (PlacesListInterface) Naming.lookup(s);
				l.addPlace(p);
			}
			r = (ObjectRegistryInterface) Naming.lookup("rmi://localhost:2023/registry");
			r.addRManager(p.getPostalCode(), "rmi://localhost:2024/replicamanager");
		} catch (MalformedURLException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized String getPlaceListAddress(String id) throws RemoteException {
		if(placesList.isEmpty()) return null;
		return placesList.get(ThreadLocalRandom.current().nextInt(0, placesList.size()));
	}

	@Override
	public synchronized String addReplica(String replicaAddress) throws RemoteException {
		System.out.println("ReplicaManager-->Adicionei uma Réplica");
		String add = getPlaceListAddress(null);
		this.placesList.add(replicaAddress);
		return add;
	}

	@Override
	public synchronized void removeReplica(String replicaAddress) throws RemoteException {
		this.placesList.remove(replicaAddress);
	}
}
